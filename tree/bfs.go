package main

import (
	"container/list"
)

type Node1 struct {
	data  int
	left  *Node1
	right *Node1
}

func main() {
	var root *Node1
	root = nil
	root = &Node1{data: 1}
	root.left = &Node1{data: 2, left: &Node1{data: 4}, right: &Node1{data: 5}}
	root.right = &Node1{data: 3}
	//root.left.right = root.right //tree to graph
	traverseTree(root)
}

func traverseTree(root *Node1) {
	visited := make(map[int]*Node1)
	queue := list.New()
	visited[root.data] = root
	queue.PushBack(root)
	for queue.Len() != 0 {
		if queue.Front().Value.(*Node1).left != nil {
			if _, ok := visited[queue.Front().Value.(*Node1).left.data]; !ok {
				queue.PushBack(queue.Front().Value.(*Node1).left)
			}
		}
		if queue.Front().Value.(*Node1).right != nil {
			queue.PushBack(queue.Front().Value.(*Node1).right)
		}
		print(queue.Front().Value.(*Node1).data, " ")
		queue.Remove(queue.Front())
	}
}
