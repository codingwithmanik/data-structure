package main

type Node struct {
	data  int
	left  *Node
	right *Node
}

func main() {
	var root *Node
	root = nil

	root = &Node{data: 1}
	root.left = &Node{data: 2, left: &Node{data: 4}, right: &Node{data: 5}}
	root.right = &Node{data: 3}
	print("PreOrder: ")
	preOrder(root)
	print("\nInOrder: ")
	inOrder(root)
	print("\nPostOrder: ")
	postOrder(root)
}

func preOrder(root *Node) {
	if root == nil {
		return
	}
	print(root.data, " ")
	preOrder(root.left)
	preOrder(root.right)
}
func inOrder(root *Node) {
	if root == nil {
		return
	}
	inOrder(root.left)
	print(root.data, " ")
	inOrder(root.right)
}

func postOrder(root *Node) {
	if root == nil {
		return
	}
	postOrder(root.left)
	postOrder(root.right)
	print(root.data, " ")
}
