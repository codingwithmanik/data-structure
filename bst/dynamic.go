package main

import (
	"container/list"
	"fmt"
	"math"
)

type BST struct {
	root *Node
}
type Node struct {
	data  int
	left  *Node
	right *Node
}

func main() {
	//bst := BST{}
	var root *Node
	root = nil
	root = insert(root, 15) //O(logn)
	root = insert(root, 10)
	root = insert(root, 20)
	root = insert(root, 25)
	root = insert(root, 8)
	root = insert(root, 12)
	root = insert(root, 7)

	fmt.Println("Min(Iterative) = ", findMin(root))
	fmt.Println("Min(Recursion) = ", findMinRecursion(root))
	fmt.Println("Max(Iterative) = ", findMax(root))

	fmt.Println("Height = ", findHeight(root))           //O(n)
	fmt.Println("Depth  = ", findDepth(root, 8))         //O(logn)
	fmt.Println("Depth of Tree equal to height of tree") //O(n)

	levelOrderTraverse(root) //BFS O(n)
	fmt.Print("Visited Nodes Are(PreOrder): ")
	preOrderTraverse(root) //DFS O(n)
	fmt.Print("\nVisited Nodes Are(InOrder): ")
	inOrderTraverse(root) //DFS O(n) This will return data as sorted order if it is BST
	fmt.Print("\nVisited Nodes Are(PostOrder): ")
	postOrderTraverse(root) //DFS O(n)

	fmt.Println("\nIs binary Search Tree : ", isBinarySearchTree(root)) //too much expensive in terms of time complexity: O(n^2)
	fmt.Println("Is binary Search Tree : ", isBinarySearchTree2(root))  //O(n)
	//we can use inorder approach also, if data are sorted then we can say the tree is BST

	fmt.Println("Enter a value to search")
	a := 0
	valid, _ := fmt.Scan(&a)
	if valid == 1 {
		if search(root, a) { //O(logn)
			fmt.Println("Found")
		} else {
			fmt.Println("Not Found")
		}
	}
}

func postOrderTraverse(root *Node) {
	if root == nil {
		return
	}
	postOrderTraverse(root.left)
	postOrderTraverse(root.right)
	fmt.Print(root.data, " ")
}

func inOrderTraverse(root *Node) {
	if root == nil {
		return
	}
	inOrderTraverse(root.left)
	fmt.Print(root.data, " ")
	inOrderTraverse(root.right)
}

func preOrderTraverse(root *Node) {
	if root == nil {
		return
	}
	fmt.Print(root.data, " ")
	preOrderTraverse(root.left)
	preOrderTraverse(root.right)
}

func levelOrderTraverse(root *Node) {
	fmt.Print("Visited Nodes are: ")
	if root == nil {
		return
	}
	queue := list.New()
	queue.PushBack(root)
	for queue.Len() != 0 {
		current, ok := queue.Front().Value.(*Node)
		if ok {
			fmt.Print(current.data, " ")
			if current.left != nil {
				queue.PushBack(current.left)
			}
			if current.right != nil {
				queue.PushBack(current.right)
			}
			queue.Remove(queue.Front())
		}
	}
	fmt.Println()
}

func findDepth(root *Node, data int) int {
	if root == nil {
		return -1
	}
	if root.data == data {
		return 0
	}
	if data <= root.data {
		return findDepth(root.left, data) + 1
	} else {
		return findDepth(root.right, data) + 1
	}
}

func findHeight(root *Node) int {
	if root == nil {
		return -1
	}
	leftHeight := findHeight(root.left)
	rightHeight := findHeight(root.right)
	return int(math.Max(float64(leftHeight), float64(rightHeight)) + 1)
}

func findMin(root *Node) int {
	if root == nil {
		fmt.Println("Tree is empty!")
		return -1
	}
	for root.left != nil {
		root = root.left
	}
	return root.data
}
func findMinRecursion(root *Node) int {
	if root == nil {
		fmt.Println("Tree is empty!")
		return -1
	} else if root.left == nil {
		return root.data
	}
	return findMinRecursion(root.left)
}

func findMax(root *Node) int {
	if root == nil {
		fmt.Println("Tree is empty!")
		return -1
	}
	for root.right != nil {
		root = root.right
	}
	return root.data
}

func insert(root *Node, data int) *Node {
	if root == nil {
		root = getNewNode(data)
	} else if data <= root.data {
		root.left = insert(root.left, data)
	} else {
		root.right = insert(root.right, data)
	}
	return root
}

func search(root *Node, data int) bool {
	if root == nil {
		return false
	} else if root.data == data {
		return true
	} else if data <= root.data {
		return search(root.left, data)
	} else {
		return search(root.right, data)
	}
}

func isBinarySearchTree2(root *Node) bool {
	return isBstUtil(root, math.MinInt, math.MaxInt)
}

func isBstUtil(root *Node, minValue int, maxValue int) bool {
	if root == nil {
		return true
	}
	if root.data > minValue &&
		root.data < maxValue &&
		isBstUtil(root.left, minValue, root.data) &&
		isBstUtil(root.right, root.data, maxValue) {
		return true
	} else {
		return false
	}
}

func isBinarySearchTree(root *Node) bool {
	if root == nil {
		return true
	}
	if isSubtreeLesser(root.left, root.data) &&
		isSubtreeGreater(root.right, root.data) &&
		isBinarySearchTree(root.left) &&
		isBinarySearchTree(root.right) {
		return true
	} else {
		return false
	}
}

func isSubtreeLesser(root *Node, data int) bool {
	if root == nil {
		return true
	}
	if root.data <= data &&
		isSubtreeLesser(root.left, data) &&
		isSubtreeLesser(root.right, data) {
		return true
	} else {
		return false
	}
}
func isSubtreeGreater(root *Node, data int) bool {
	if root == nil {
		return true
	}
	if root.data > data &&
		isSubtreeGreater(root.left, data) &&
		isSubtreeGreater(root.right, data) {
		return true
	} else {
		return false
	}
}

func getNewNode(data int) *Node {
	node := &Node{
		data:  data,
		left:  nil,
		right: nil,
	}
	return node
}
