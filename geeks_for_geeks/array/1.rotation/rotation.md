# Basic Rotation

#### Problem: Given an array of integers arr[] of size N and an integer, the task is to rotate the array elements to the left by d positions.
Approach 1 (Using temp array):
- After rotating d positions to the left, the first d elements become the last d elements of the array
- First store the elements from index d to N-1 into the temp array.
- Then store the first d elements of the original array into the temp array.
- Copy back the elements of the temp array into the original array
``` 
    //left rotation
	arr := []int{1, 2, 3, 4, 5, 6, 7}
	d := 2
	temp := arr[2:]
	for i := 0; i < d; i++ {
		temp = append(temp, arr[i])
	}
	fmt.Println(temp)
	
    //right rotation
	size := len(arr)
	r := size - d%size
	arr = append(arr[r:], arr[:r]...)
	fmt.Println(arr)
```
- Time complexity: O(N)
- Auxiliary Space: O(N)

Approach 2 (Rotate one by one):
- At each iteration, shift the elements by one position to the left circularly (i.e., first element becomes the last).
- Perform this operation d times to rotate the elements to the left by d position.
```
	arr := []int{1, 2, 3, 4, 5, 6, 7}
	d := 2
	for d != 0 {
		temp := arr[0]
		for i := 0; i < len(arr)-1; i++ {
			arr[i] = arr[i+1]
		}
		arr[len(arr)-1] = temp
		d--
	}
	fmt.Println(arr)
```
- Time Complexity: O(N * d)
- Auxiliary Space: O(1)

Approach 3 (A Juggling Algorithm): This is an extension of method 2


