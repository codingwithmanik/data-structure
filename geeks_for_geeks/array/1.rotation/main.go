package main

import "fmt"

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7}
	size := len(arr)
	d := 2
	r := size - d%size
	arr = append(arr[r:], arr[:r]...)
	fmt.Println(arr)

	fmt.Println("\nLCM:")
	fmt.Println(LCM(10, 15))
	fmt.Println(LCM(10, 15, 20))
	fmt.Println(LCM(1, 2, 3, 4, 5, 6, 7, 8, 9, 10))
}

// GCD Euclidean Algorithm
func GCD(a, b int) int {
	//if b == 0 {
	//	return a
	//}
	//return GCD(b, a%b)
	for b != 0 {
		t := b
		b = a % b
		a = t
	}
	return a
}

func LCM(a, b int, integers ...int) int {
	result := a * b / GCD(a, b)

	for i := 0; i < len(integers); i++ {
		result = LCM(result, integers[i])
	}

	return result
}
