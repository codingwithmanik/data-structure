package main

import "fmt"

type Node1 struct {
	data int
	next *Node1
}

func main() {
	Head := Node1{}
	temp := Node1{1, nil}
	Head = temp
	insertAtEnd(&Head, 2)
	insertAtEnd(&Head, 3)
	printList1(&Head)
}
func insertAtEnd(head *Node1, data int) {
	newNode := Node1{data, nil}
	for head.next != nil {
		head = head.next
	}
	head.next = &newNode
}

func printList1(head *Node1) {
	for head != nil {
		fmt.Println(head)
		head = head.next
	}
}
