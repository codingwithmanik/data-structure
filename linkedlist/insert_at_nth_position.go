package main

import "fmt"

type Node2 struct {
	data int
	next *Node2
}

func main() {
	Head := &Node2{}
	temp := &Node2{1, nil}
	Head = temp
	//We added return to below function because of not existing pass by reference in golang
	if h := insertAtnthPosition(Head, 2, 1); h != nil {
		Head = h
	}
	if h := insertAtnthPosition(Head, 3, 2); h != nil {
		Head = h
	}
	if h := insertAtnthPosition(Head, 8, 3); h != nil {
		Head = h
	}
	if h := insertAtnthPosition(Head, 50, 2); h != nil {
		Head = h
	}
	if h := insertAtnthPosition(Head, 100, 1); h != nil {
		Head = h
	}
	printList2(Head)
}
func insertAtnthPosition(head *Node2, data int, position int) *Node2 {
	newNode := &Node2{}
	newNode.data = data
	newNode.next = nil
	if position == 1 {
		newNode.next = head
		return newNode
	}
	temp2 := head
	for i := 0; i < position-2; i++ {
		temp2 = temp2.next
	}
	newNode.next = temp2.next
	temp2.next = newNode
	return nil
}

func printList2(head *Node2) {
	for head != nil {
		fmt.Println(head)
		head = head.next
	}
}
