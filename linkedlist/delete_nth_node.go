package main

import "fmt"

type DList struct {
	head *DNode
}
type DNode struct {
	data int
	next *DNode
}

func main() {
	list := &DList{}
	temp := &DNode{
		1, nil,
	}
	list.head = temp
	list.insert(4)
	list.insert(7)
	list.insert(10)
	fmt.Println("before delete===============> ")
	printListD(list.head)
	list.delete_nthNode(2)
	fmt.Println("after delete================> 2th position")
	printListD(list.head)
	list.delete_nthNode(1)
	fmt.Println("after delete================> 1th position")
	printListD(list.head)
}
func (list *DList) delete_nthNode(position int) {
	if position == 1 {
		list.head = list.head.next
		return
	}
	temp1 := list.head
	for i := 0; i < position-2; i++ {
		temp1 = temp1.next
	}
	temp2 := temp1.next
	temp1.next = temp2.next
	//free temp2
	temp2 = nil
}

func (list *DList) insert(data int) {
	newNode := DNode{data, nil}
	node := list.head
	for node.next != nil {
		node = node.next
	}
	node.next = &newNode
}

func printListD(head *DNode) {
	for head != nil {
		fmt.Println(head)
		head = head.next
	}
}
