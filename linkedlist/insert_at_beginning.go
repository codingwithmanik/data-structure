package main

import "fmt"

type Node struct {
	data int
	next *Node
}

func main() {
	Head := &Node{}
	temp := &Node{1, nil}
	Head = temp
	Head = insertAtBeginning(Head, 2)
	Head = insertAtBeginning(Head, 3)
	printList(Head)
}

func insertAtBeginning(head *Node, data int) *Node {
	newNode := Node{data, head}
	return &newNode
}
func printList(head *Node) {
	for head != nil {
		fmt.Println(head)
		head = head.next
	}
}
