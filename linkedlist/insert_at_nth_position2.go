package main

import "fmt"

type List struct {
	head *Node3
}
type Node3 struct {
	data int
	next *Node3
}

func main() {
	list := &List{}
	temp := &Node3{1, nil}
	list.head = temp

	list.insertAtnthPosition(2, 1)
	list.insertAtnthPosition(3, 1)
	list.insertAtnthPosition(5, 2)
	list.insertAtnthPosition(8, 3)
	list.insertAtnthPosition(50, 2)
	list.insertAtnthPosition(100, 1)
	printList3(list.head)
}
func (list *List) insertAtnthPosition(data int, position int) {
	newNode := &Node3{data, nil}
	if position == 1 {
		newNode.next = list.head
		list.head = newNode
		return
	}
	temp2 := list.head
	//if position greater than or equal 3
	for i := 0; i < position-2; i++ {
		temp2 = temp2.next
	}
	newNode.next = temp2.next
	temp2.next = newNode
}

func printList3(head *Node3) {
	for head != nil {
		fmt.Println(head)
		head = head.next
	}
}
